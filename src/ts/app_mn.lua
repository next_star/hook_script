--[[
	version: 1.0.1
	author:lancy
	date:2017年11月26日
	
	PS：
		需要配置config.lua中模拟精灵悬浮按钮的位置
		需要引用 触动精灵扩展库 sz.so 
]]
require("config")
require "TSLib"

app_mn={};
app_mn.PACKAGE_NAME="com.hms.jingling";		     -- 模拟精灵包名

local POS_RANDOM_BTN_X,POS_RANDOM_BTN_Y=255,537; -- 随机按钮
local POS_SAVE_BTN_X,POS_SAVE_BTN_Y=720,531;	 -- 写入按钮
local POS_STATUS_BG_X,POS_STATUS_BG_Y=540,150;	 -- 写入按钮


local COLOR_NORMAL=0x1a94bc;					 -- 其实状态颜色 蓝色
local COLOR_WRITED=0x1abc89;					 -- 写入状态颜色 绿色
local TIMEOUT_STARTUP=4;

local isMocked=0;


--[[
	 检查模拟精灵是否运行，如果未运行则启动
--]]
local function checkAndRun()
	
	-- 检查模拟精灵是否运行
	local isRunFlag = appIsRunning(app_mn.PACKAGE_NAME); 
	while (isRunFlag==0) do
		-- 启动模拟精灵
		local isStartUp=runApp(app_mn.PACKAGE_NAME);
		if isStartUp == 0 then
			local color=0;
			while color ~= COLOR_NORMAL do                           --如果应用处于前台则继续
				mSleep(100); 
				color = getColor(POS_STATUS_BG_X, POS_STATUS_BG_Y);    --获取(100,100)的颜色值，赋值给color变量
			end
			toast("模拟精灵启动成功");
		else
			toast("模拟精灵启动失败");
		end
		mSleep(300)
		-- 检查模拟精灵是否运行
		isRunFlag = appIsRunning(app_mn.PACKAGE_NAME); --判断应用是否启动
	end

	-- 检查模拟精灵是否处于前台
	local isFront = isFrontApp(app_mn.PACKAGE_NAME);
	
	while (isFront==0) do
		local canuse=0;
		while (canuse==0) do                           --如果应用处于前台则继续
			tap(config.app_mn.float_x,config.app_mn.float_y); --点击悬浮
			mSleep(500); 
			local color = getColor(POS_STATUS_BG_X,POS_STATUS_BG_Y);    --获取(100,100)的颜色值，赋值给color变量
			if(color==COLOR_NORMAL or color==COLOR_WRITED) then
				canuse=1;
			end

		end
		mSleep(500);
		isFront = isFrontApp(app_mn.PACKAGE_NAME); --判断应用是否启动
	end
	return true;
end


local producer = coroutine.create( function ()
		while (true) do
			local result=false;
			if(checkAndRun()) then
				tap(POS_RANDOM_BTN_X,POS_RANDOM_BTN_Y); --点击随机 
				mSleep(1000); 
				tap(POS_SAVE_BTN_X,POS_SAVE_BTN_Y); --点击写入
				
				while (true) do
					local color = getColor(POS_STATUS_BG_X,POS_STATUS_BG_Y);
					if(color==COLOR_WRITED) then
						break;
					end
				end
				tap(config.app_mn.float_x,config.app_mn.float_y); --点击悬浮
				
				result=true; 
			else
				result=false;
			end
			coroutine.yield(result)
		end 

	end)


--[[  
     模拟设备信息		
--]]
function app_mn.mock()
	local status, value = coroutine.resume(producer);
	return value
end





